# განსაზღვრეთ x და y ცვლადები. მიანიჭეთ მნიშვნელობები შესაბამისად 5 და 6. გამოიტანეთ
# ცვლადების ჯამი, ნამრავლი, სხვაობა ცალ-ცალკე სტრიქონზე
x = 5
y = 6
print(x+y)   # ჯამი
print(x*y)   #ნამრავლი
print(x-y)   #სხვაობა
