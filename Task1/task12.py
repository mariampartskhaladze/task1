# განსაზღვრეთ საჭირო რაოდენობის ცვლადები, მიანიჭეთ რაიმე მნიშვნელობები
# კლავიატურიდან და გამოიტანეთ ქვემოთ მოყვანილი გამოსახულებების გამოთვლების შედეგი:
# გამოსახულება
# 1) 3𝑎 + 6
# 2) 3𝑎 + 6𝑏 − 1
# 3) 𝑘 + 𝑡 − 𝑧 + 12
# 4) 3𝑎/4+6t
# 5) −4ℎ + 3𝑑 − 4
# 6) 3a+6/4-3b
# 7) 7𝑎^2+6c
# 8) 2a^+6c^3
# 9) 11𝑘 + 2𝑡 − 𝑝 + 12e
# 10) k+(12e/4m)
a = 1
b = 2
c = 3
d = 4
k = 5
t = 6
z = 7
h = 8
p = 9
e = 10
m = 11
# 1 3𝑎 + 6
print(str(3)+"*"+str(a)+"+"+str(6)+"="+str(3*a+6))
# 2 3𝑎 + 6𝑏 − 1
print(str(3)+"*"+str(a)+"+"+str(6)+"*"+str(b)+"-"+str(1)+"="+str(3*a+6*b-1))
# 3  𝑘 + 𝑡 − 𝑧 + 12
print(str(k)+"+"+str(t)+"-"+str(z)+"+"+str(12)+"="+str(k+t-z+12))
# 4 3𝑎/4+6t
print((str(3)+"*"+str(a)+"/"+str(4))+"+"+str(6)+"*"+str(t)+"="+str(3*a/4+6*t))
# 5 −4ℎ + 3𝑑 − 4
print(str(-4)+"*"+str(h)+"+"+str(3)+"*"+str(d)+"-"+str(4)+"="+str(-4*h+3*d-4))
# 6 3a+6/4-3b
print((str(3)+"*"+str(a)+"+"+str(6))+"/"+str(4)+"-"+str(3)+"*"+str(b)+"="+str((3*a+6)/(4-3*b)))
# 7 7𝑎^2+6c
print(str(7)+"*"+str(a**2)+"+"+str(6)+"*"+str(c)+"="+str(7*a**2+6*c))
# 8 2a^+6c^3
print(str(2)+"*"+str(a**2)+"+"+str(6)+"*"+str(c**3)+"="+str(2*a**2+6*c**3))
# 9 11𝑘 + 2𝑡 − 𝑝 + 12e
print(str(11)+"*"+str(k)+"+"+str(2)+"*"+str(t)+"-"+str(p)+"+"+str(12)+"*"+str(e)+"="+str(11*k+2*t-p+12*e))
# 10  k+(12e/4m)
print(str(k)+"+"+str(12)+"*"+str(e)+"/"+str(4)+"*"+str(m)+"="+str(k+((12*e)/(4*m))))
